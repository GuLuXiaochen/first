import { Suspense } from "react";

import { Switch, Redirect, Route } from "react-router-dom";

const RouterView = (routerlist) => {

    const routerList = routerlist.routerlist && routerlist.routerlist.filter(item => !item.to);
    const redirectItem = routerlist.routerlist && routerlist.routerlist.find(item => item.to);
    return (
        <Suspense fallback={<div>loadding</div>}>
            <Switch>
                {
                    routerList && routerList.length !== 0 ? routerList.map((item, index) => {
                        return <Route path={item.path} key={index} render={(routerProps) => {
                            let Son = item.component;
                            if (item.children) {
                                return <Son child={item.children} navlink={item.children.filter(i => !i.to)} />
                            }
                            return <Son {...routerProps} />
                        }}></Route>
                    }) : <div>暂无视图</div>
                }
                {
                    redirectItem && <Redirect to={redirectItem.to} from={redirectItem.from}></Redirect>
                }
            </Switch>
        </Suspense>
    )
}

export default RouterView