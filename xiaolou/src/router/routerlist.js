import { lazy } from "react";

const routerlist = [
    {
        path: "/home",
        component: lazy(() => import("../views/home")),
        children: [
            {
                path: "/home/article",
                name: "文章",
                component: lazy(() => import("../views/article"))
            },
            {
                path: "/home/pigeonhole",
                name: "归档",
                component: lazy(() => import("../views/pigeonhole"))
            },
            {
                path: "/home/knowledge",
                name: "知识小册",
                component: lazy(() => import("../views/knowledge"))
            },
            {
                path: "/home/messageboard",
                name: "留言板",
                component: lazy(() => import("../views/messageboard"))
            },
            {
                path: "/home/into",
                name: "关于",
                component: lazy(() => import("../views/into"))
            },
            {
                to: "/home/article",
                from: "/home"
            }
        ]
    },
    {
        to: "/home",
        from: "/"
    }
]

export default routerlist
