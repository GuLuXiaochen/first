import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router } from "react-router-dom"
import RouterView from "./router/RouterView"
import routerlist from "./router/routerlist"
import { Provider } from "react-redux"
import store from "./store"
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RouterView routerlist={routerlist}></RouterView>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
