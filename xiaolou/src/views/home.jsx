import React, { Component } from 'react'

import Navigation from '../components/Navigation'

import RouterView from '../router/RouterView'

export default class home extends Component {
    render() {
        let { child, navlink } = this.props
        return (
            <div>
                <Navigation navlink={navlink}></Navigation>

                <RouterView routerlist={child}></RouterView>

            </div>
        )

    }
}
