import React, { Component } from 'react'
import { NavLink } from "react-router-dom"
export default class Navigation extends Component {
    render() {
        let { navlink } = this.props;

        return (
            <div>
                {
                    navlink && navlink.length !== 0 ? navlink.map((item, index) => {
                        return <NavLink key={index} to={item.path}>{item.name}</NavLink>
                    }) : <div>暂无视图</div>
                }
            </div>
        )
    }
}
