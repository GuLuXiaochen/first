import { combineReducers, createStore, applyMiddleware } from "redux"

import articleReducer from "./reducer/articleReducer"

import thunk from "redux-thunk"

import logger from "redux-logger"

const reducer = combineReducers({ articleReducer });

export default createStore(reducer, applyMiddleware(thunk, logger))